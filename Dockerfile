FROM java:8
LABEL maintainer="dylanalves@hotmail.com"
WORKDIR /app
COPY target/shorty-application.jar /app/shorty-application.jar
ENTRYPOINT ["java", "-jar", "shorty-application.jar"]