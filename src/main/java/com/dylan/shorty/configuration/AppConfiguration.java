package com.dylan.shorty.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Getter;
import lombok.Setter;

@Configuration
@PropertySource("classpath:application.properties")
@ConfigurationProperties(prefix = "shorty")
@Getter
@Setter
public class AppConfiguration {
	
	private String client;
	private String secret;
}
