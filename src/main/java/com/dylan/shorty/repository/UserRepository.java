package com.dylan.shorty.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.dylan.shorty.document.User;

public interface UserRepository extends MongoRepository<User, String>{

	User findByUsername(String username);
}
