package com.dylan.shorty.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.dylan.shorty.document.Url;

public interface UrlRepository extends MongoRepository<Url, String>{

}
