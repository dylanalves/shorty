package com.dylan.shorty.service;

import java.util.Optional;

import com.dylan.shorty.document.Url;


public interface ShortenerService {

	Optional<Url> findById(String id);
	Url save (Url object);

}
