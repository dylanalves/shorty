package com.dylan.shorty.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dylan.shorty.document.Url;
import com.dylan.shorty.repository.UrlRepository;


@Service
public class ShortenerServiceImpl implements ShortenerService{

	@Autowired
	private UrlRepository repository;
	
	@Override
	public Optional<Url> findById(String id) {
		return repository.findById(id);
	}

	@Override
	public Url save(Url object) {
		return repository.save(object);
	}
}
