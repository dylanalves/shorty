package com.dylan.shorty.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dylan.shorty.document.User;
import com.dylan.shorty.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository repository;
	
	@Override
	public User save(User user) {
		return repository.save(user);
	}
	
	@Override
	public User findByUsername(String username) {
		return repository.findByUsername(username);
	}
}
