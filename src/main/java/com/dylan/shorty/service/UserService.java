package com.dylan.shorty.service;

import com.dylan.shorty.document.User;

public interface UserService {

	User save(User user);
	User findByUsername(String username);
}
