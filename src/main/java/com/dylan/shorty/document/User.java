package com.dylan.shorty.document;

import java.util.List;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Document
@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
public class User {
	
	@Indexed(unique=true)
	private String username;
	private String password;
	private List<Role> roles;
}
