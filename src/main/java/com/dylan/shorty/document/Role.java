package com.dylan.shorty.document;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Document
@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
public class Role {

	private String name;
	
	public Role(String name) {
		super();
		this.name = name;
	}
}
