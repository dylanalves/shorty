package com.dylan.shorty.resource;

import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dylan.shorty.document.Url;
import com.dylan.shorty.service.ShortenerService;
import com.google.common.hash.Hashing;

@RestController
public class ShortenerResource {

	@Autowired
	private ShortenerService service;
	
	@GetMapping("api/{hash}")
	public ResponseEntity<Void> getUrl(@PathVariable String hash) throws IOException {
		
		Optional<Url> url = service.findById(hash);
		
		if(url.isPresent()) {
			HttpHeaders headers = new HttpHeaders();
			headers.setLocation(URI.create(url.get().getUrl()));
			return new ResponseEntity<>(headers, HttpStatus.MOVED_PERMANENTLY);
		}else {
			throw new RuntimeException("No such URL was found");
		}
	}
	
	@PostMapping("/api")
	public Object shortUrl(@RequestBody Url url, HttpServletRequest request) {
		
		UrlValidator validator = new UrlValidator(new String[]{"http", "https"});
		
		if(validator.isValid(url.getUrl().trim())) {
			String mutatedUrl = Hashing.murmur3_32().hashString(url.getUrl(),  StandardCharsets.UTF_8).toString();
			
			if(service.findById(mutatedUrl).isPresent()) {
				return request.getRequestURL().toString() + "/" + mutatedUrl;
			}
			else {
				url.setId(mutatedUrl);
				return request.getRequestURL().toString() + "/" + service.save(url).getId();
			}
		}
		else {
			throw new RuntimeException("Your URL must contains 'http' or 'https' to be shorten");
		}
	}
}
