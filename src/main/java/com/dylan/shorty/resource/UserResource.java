package com.dylan.shorty.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dylan.shorty.document.User;
import com.dylan.shorty.service.UserService;


@RestController
public class UserResource {

	@Autowired
	private UserService service;
	
	@PostMapping("/api/user")
	public Object save(@RequestBody User user) {
		if(service.findByUsername(user.getUsername()) == null) {
			return service.save(user);
		}else {
			return "Username '" + user.getUsername() + "', already exists.\nPlease, consider use any other username.";
		}
		
	}
}
