package com.dylan.shorty;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.dylan.shorty.component.OauthHelper;
import com.dylan.shorty.document.Url;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ShortenerResourceTest {

	private MockMvc mockMvc;	
		
	@Autowired
    private WebApplicationContext context;
	
	@Autowired
	private OauthHelper helper;
	
    private ObjectMapper objectMapper;
	
	@Before
	public void setUp() throws Exception {		
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}

	@Test
	public void testShortUrl() throws Exception {
		objectMapper = new ObjectMapper();
		
		Url url = new Url();
		url.setUrl("https://www.crunchyroll.com");
		
		mockMvc.perform(post("/api")
				.with(helper.getToken("shorty_tester", "TESTER"))
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(url))
				.accept(MediaType.APPLICATION_JSON_UTF8))
		.andExpect(status().isOk());
	}
	
	@Test
	public void testGetUrl() throws Exception {
		objectMapper = new ObjectMapper();
		
		Url url = new Url();
		url.setUrl("https://www.crunchyroll.com");
		
		String urlHash = "";
		urlHash = mockMvc.perform(post("/api")
								.with(helper.getToken("shorty_tester", "TESTER"))
								.contentType(MediaType.APPLICATION_JSON_UTF8)
								.content(objectMapper.writeValueAsString(url))
								.accept(MediaType.APPLICATION_JSON_UTF8))
						.andExpect(status().isOk()).andReturn().getResponse().getContentAsString().split("/api/")[1];
		
		mockMvc.perform(get("/api/{hash}", urlHash))
			   .andExpect(status().is3xxRedirection());
	}
}
