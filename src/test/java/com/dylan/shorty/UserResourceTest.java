package com.dylan.shorty;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.dylan.shorty.document.Role;
import com.dylan.shorty.document.User;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserResourceTest {

    private ObjectMapper objectMapper;
    
	private MockMvc mockMvc;	
	
	@Autowired
    private WebApplicationContext context;
	
	@Before
	public void setUp() throws Exception {		
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}
	
	@Test
	public void testSave() throws Exception {
		objectMapper = new ObjectMapper();
		
		User user = new User("api_tester", "password", Arrays.asList(new Role("USER"), new Role("TESTER")));
		
		mockMvc.perform(post("/api/user")
						.contentType(MediaType.APPLICATION_JSON_UTF8)
						.content(objectMapper.writeValueAsString(user))
						.accept(MediaType.APPLICATION_JSON_UTF8))
		       .andExpect(status().isOk());
	}

}
