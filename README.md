# [Shorty](https://com-shorty.herokuapp.com/)

A URL shortener Application

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Software you must have installed 


- [Java 8](https://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven](https://maven.apache.org/install.html)
- [Postman](https://www.getpostman.com/) *for resource testing purposes*
- [Docker](https://www.docker.com/) *optional*


### Installing

On project's directory...


##### Building jar

First, let's create application's .jar file, so we can use it later along with Java or Docker


> $ mvn clean install

Once it's complete, is possible to find .jar file on target/shorty-application.jar


##### Run with Maven

To run this project with Maven, you can use:


> $ mvn spring-boot:run



##### Run with Java

To run this project with Java, you can use:


> $ java -jar target/shorty-application.jar



##### Run with Docker

This project is compatible with Docker, so, is possible running it using:


> $ docker-compose build

Once the image is created by docker, run:


> $ docker-compose up

## Before we play, we need to know

* This project is secure with Oauth2 by [Spring Security](https://spring.io/projects/spring-security).

* Even though implements authentication, this project doesn't contemplates authentication filtering based on ROLES.
  So, when you are about to save a new user, don't worry much with which Role fill in. 
  I suggest some, though.

* This project uses inMemory Client.

* Yes, you realized it as well, don't you? There wasn't need to configure a Database, even with Docker Compose.
  That's because, this project uses a Cloud Service for storage, known as [MongoDB Atlas](https://cloud.mongodb.com)

* As a URL shortener, some resources are exposed with no security, like create a new user, or use the short url (provides a **GET** request).

* Postman JSON import files are available for tests on local and cloud environment.

## Time to Play

After deployed this project correctly, it will be acessible on **localhost:8080/**


#### New User

First of all, let's insert a new user. 
And for that, the Endpoint **/api/user** is exposed for the following post request:

```cURL
    curl -X POST \
    http://localhost:8080/api/user \
    -H 'Content-Type: application/json' \
    -H 'cache-control: no-cache' \
    -d '{
        "username": "user",
        "password": "password",
        "roles":[{
                "name": "USER"
            }]
    }'
```

##### Roles
* USER
* TESTER
* DEVELOPER


#### Get Token

With the user you just create, is time to get a Token.
On Authorization, is necessary to use the cliend_id and client_secret encoded, that respectively, are:

*shorty_api*  
*shorty*

For reference, import postman json file and use it as the client to your tests.

```cURL
    curl -X POST \
    http://localhost:8080/oauth/token \
    -H 'Authorization: Basic c2hvcnR5X2FwaTpzaG9ydHk=' \
    -H 'Content-Type: application/x-www-form-urlencoded' \
    -H 'cache-control: no-cache' \
    -d 'grant_type=client_credentials&username=user&password=password'
```

### Short my first URL

Now you have collected the Bearer Token, you can short a url in this endpoint **/api**, like so:

```cURL
    curl -X POST \
    http://localhost:8080/api \
    -H 'Authorization: Bearer cc22bc25-b529-4793-96f1-565234a31c6d' \
    -H 'Content-Type: application/json' \
    -H 'cache-control: no-cache' \
    -d '{
        "url": "https://www.some-url.com"
    }'
```

This will return a hashed url, like: http://localhost:8080/api/xxxxx

### Access my Original URL

Use the hashed URL on browser, and it will redirect you to the address of the original URL.

Of course, you can also use:

```cUrl
    curl -X GET \
    http://localhost:8080/api/xxxxxx \
    -H 'Content-Type: application/json' \
    -H 'cache-control: no-cache'
```

## Testing

To run this project's tests apart from install, use:


> $ mvn clean test



## Built With

* [Spring Boot](https://spring.io/projects/spring-boot) - The framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [MongoDB Atlas](https://www.mongodb.com/cloud/atlas) - Cloud MongoDB service
* [Guava](https://github.com/google/guava) - Used to generate URL hash
* [Commons Validator](https://commons.apache.org/proper/commons-validator/) - Used to validate inputed URL



## Author

* **Dylan Alves da Silva** - [Dylan Alves](https://bitbucket.org/dylanalves/)


## Acknowledgments

* Study case for my Java knowlodge
